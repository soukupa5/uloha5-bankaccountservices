/**
 * BankServicesService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package bank_services;

public interface BankServicesService extends javax.xml.rpc.Service {
    public java.lang.String getBankServicesPortAddress();

    public bank_services.BankServices getBankServicesPort() throws javax.xml.rpc.ServiceException;

    public bank_services.BankServices getBankServicesPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
