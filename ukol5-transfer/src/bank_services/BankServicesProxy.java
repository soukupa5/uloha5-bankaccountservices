package bank_services;

public class BankServicesProxy implements bank_services.BankServices {
  private String _endpoint = null;
  private bank_services.BankServices bankServices = null;
  
  public BankServicesProxy() {
    _initBankServicesProxy();
  }
  
  public BankServicesProxy(String endpoint) {
    _endpoint = endpoint;
    _initBankServicesProxy();
  }
  
  private void _initBankServicesProxy() {
    try {
      bankServices = (new bank_services.BankServicesServiceLocator()).getBankServicesPort();
      if (bankServices != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bankServices)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bankServices)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bankServices != null)
      ((javax.xml.rpc.Stub)bankServices)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public bank_services.BankServices getBankServices() {
    if (bankServices == null)
      _initBankServicesProxy();
    return bankServices;
  }
  
  public boolean checkAccountExists(java.lang.String arg0) throws java.rmi.RemoteException{
    if (bankServices == null)
      _initBankServicesProxy();
    return bankServices.checkAccountExists(arg0);
  }
  
  public boolean checkAccountBalance(java.lang.String arg0, double arg1) throws java.rmi.RemoteException{
    if (bankServices == null)
      _initBankServicesProxy();
    return bankServices.checkAccountBalance(arg0, arg1);
  }
  
  public double changeBalance(java.lang.String arg0, java.lang.String arg1, double arg2) throws java.rmi.RemoteException{
    if (bankServices == null)
      _initBankServicesProxy();
    return bankServices.changeBalance(arg0, arg1, arg2);
  }
  
  
}