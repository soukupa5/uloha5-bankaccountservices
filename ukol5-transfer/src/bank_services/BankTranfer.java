package bank_services;
import java.rmi.RemoteException;

import javax.jws.WebService;

@WebService
public class BankTranfer {
	public boolean transfer(String from, String to, double amount){
		BankServicesProxy proxy = new BankServicesProxy();
		try{
			if (!proxy.checkAccountExists(from) || !proxy.checkAccountExists(to)) return false;
			if (!proxy.checkAccountBalance(from, amount)) return false;
			proxy.changeBalance(from, "-", amount);
			proxy.changeBalance(to, "+", amount);
		}
		catch (RemoteException e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
