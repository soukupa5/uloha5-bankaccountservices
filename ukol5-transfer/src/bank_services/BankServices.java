/**
 * BankServices.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package bank_services;

public interface BankServices extends java.rmi.Remote {
    public boolean checkAccountExists(java.lang.String arg0) throws java.rmi.RemoteException;
    public boolean checkAccountBalance(java.lang.String arg0, double arg1) throws java.rmi.RemoteException;
    public double changeBalance(java.lang.String arg0, java.lang.String arg1, double arg2) throws java.rmi.RemoteException;
}
