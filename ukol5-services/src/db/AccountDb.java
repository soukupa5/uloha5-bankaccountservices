package db;

import java.util.ArrayList;

public class AccountDb {
	private ArrayList<BankAccount> accounts;
	private static AccountDb instance;
	
	public AccountDb() {
		accounts = new ArrayList<BankAccount>();
		accounts.add(new BankAccount("1234", 10));
		accounts.add(new BankAccount("5678", 66));
		accounts.add(new BankAccount("9876", 120));
	}
	public static AccountDb getInstance(){
		if (instance == null) {instance = new AccountDb(); return instance;}
		else return instance;
	}
	public boolean  validBankAccountNumber(String number){
		for (BankAccount tmp : accounts){
			if(tmp.getAccountNumber().equals(number)) return true;
		}
		return false;
	}
	public boolean validBalanceForAccountNumber(double bal, String number){
		for (BankAccount tmp : accounts){
			if (tmp.getAccountNumber().equals(number)) return (bal <= tmp.getBalance());
		}
		return false;
	}
	public double changeBalanceOnAccountNumber (String number, String operator, double amount){
		for (BankAccount tmp : accounts){
			if (tmp.getAccountNumber().equals(number)) {return tmp.changeBalance(operator, amount);}
		}
		return -1;
	}
}
