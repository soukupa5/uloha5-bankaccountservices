package db;

public class BankAccount{
	private String accountNumber;
	private double balance;
	public BankAccount(String number, double bal){
		this.accountNumber = number;
		this.balance = bal;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
/*	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
*/	public double getBalance() {
		return balance;
	}
	public double changeBalance(String operator,double amount){
		if (operator.equals("+")) {this.balance += amount; return this.balance;}
		else if (operator.equals("-")) {this.balance -=amount;return this.balance;}
		return this.balance;
	}
/*	public void setBalance(double balance) {
		this.balance = balance;
	}
*/	
}

