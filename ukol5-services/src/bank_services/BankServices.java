package bank_services;

import javax.jws.WebService;

import db.AccountDb;

@WebService
public class BankServices {
	public boolean checkAccountExists(String accountNumber){
		return AccountDb.getInstance().validBankAccountNumber(accountNumber);
	}
	public boolean checkAccountBalance(String accountNumber, double balance){
		return AccountDb.getInstance().validBalanceForAccountNumber(balance, accountNumber);
	}
	public double changeBalance(String accountNumber, String operator, double amount){
		return AccountDb.getInstance().changeBalanceOnAccountNumber(accountNumber, operator, amount);
	}



}
